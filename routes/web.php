<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\KegiatanController;
use App\Http\Controllers\Admin\KompetensiController;
use App\Http\Controllers\Admin\MahasiswaController;
use App\Http\Controllers\Admin\PengajuanController;
use App\Http\Controllers\Admin\PrestasiController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Mahasiswa\CetakSKPIController;
use App\Http\Controllers\Mahasiswa\HomeController;
use App\Http\Controllers\Mahasiswa\PengajuanKegiatanController;
use App\Http\Controllers\Mahasiswa\PengajuanKompetensiController;
use App\Http\Controllers\Mahasiswa\PengajuanPrestasiController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [AuthController::class, 'login'])->middleware('guest')->name('login');
Route::post('/login', [AuthController::class, 'store'])->middleware('guest')->name('login.store');
Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth')->name('logout');



//mahasiswa
Route::prefix('mahasiswa')->middleware(['auth', 'role:mahasiswa'])->group(function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('/daftar-pengajuan', [HomeController::class, 'daftar_pengajuan'])->name('daftar-pengajuan');
    Route::get('/pengajuan-skpi', [HomeController::class, 'pengajuan_skpi'])->name('pengajuan-skpi');
    //pengajuan kegiatan
    Route::post('/pengajuan-kegiatan/store', [PengajuanKegiatanController::class, 'store'])->name('pengajuan-kegiatan.store');
    Route::get('/pengajuan-kegiatan/edit/{id}', [PengajuanKegiatanController::class, 'edit'])->name('pengajuan-kegiatan.edit');
    Route::patch('/pengajuan-kegiatan/update/{id}', [PengajuanKegiatanController::class, 'update'])->name('pengajuan-kegiatan.update');
    Route::delete('/pengajuan-kegiatan/destroy/{id}', [PengajuanKegiatanController::class, 'destroy'])->name('pengajuan-kegiatan.destroy');
    //pengajuan prestasi
    Route::post('/pengajuan-prestasi/store', [PengajuanPrestasiController::class, 'store'])->name('pengajuan-prestasi.store');
    Route::get('/pengajuan-prestasi/edit/{id}', [PengajuanPrestasiController::class, 'edit'])->name('pengajuan-prestasi.edit');
    Route::patch('/pengajuan-prestasi/update/{id}', [PengajuanPrestasiController::class, 'update'])->name('pengajuan-prestasi.update');
    Route::delete('/pengajuan-prestasi/destroy/{id}', [PengajuanPrestasiController::class, 'destroy'])->name('pengajuan-prestasi.destroy');
    //pengajuan kompetensi
    Route::post('/pengajuan-kompetensi/store', [PengajuanKompetensiController::class, 'store'])->name('pengajuan-kompetensi.store');
    Route::get('/pengajuan-kompetensi/edit/{id}', [PengajuanKompetensiController::class, 'edit'])->name('pengajuan-kompetensi.edit');
    Route::patch('/pengajuan-kompetensi/update/{id}', [PengajuanKompetensiController::class, 'update'])->name('pengajuan-kompetensi.update');
    Route::delete('/pengajuan-kompetensi/destroy/{id}', [PengajuanKompetensiController::class, 'destroy'])->name('pengajuan-kompetensi.destroy');
    //cetak
    Route::get('/cetak-skpi', [CetakSKPIController::class, 'index'])->name('cetak-skpi');
    Route::get('/cetak-skpi/pdf', [CetakSKPIController::class, 'pdf'])->name('cetak-skpi.pdf');
});

//admin
Route::prefix('admin')->middleware(['auth', 'role:admin'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');
    //pengajuan skpi
    Route::resource('kegiatan', KegiatanController::class);
    Route::resource('prestasi', PrestasiController::class);
    Route::resource('kompetensi', KompetensiController::class);
    //mahasiswa
    Route::resource('mahasiswa', MahasiswaController::class);
    Route::post('/mahasiswa/import', [MahasiswaController::class, 'import'])->name('mahasiswa.import');
    Route::get('/mahasiswa/{id}/cetak-skpi', [MahasiswaController::class, 'cetak_skpi'])->name('mahasiswa.cetak-skpi');

    //pengguna
    Route::resource('user', UserController::class);
});
