<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kegiatan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mahasiswa_id');
            $table->text('judul');
            $table->text('judul_english');
            $table->string('tingkat');
            $table->string('partisipasi');
            $table->string('penyelenggara');
            $table->string('tahun');
            $table->string('file');
            $table->string('url')->nullable();
            $table->string('status_pengajuan');
            $table->string('status_validasi');
            $table->text('komentar')->nullable();
            $table->timestamps();

            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswa')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kegiatan');
    }
};
