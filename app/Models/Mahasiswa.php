<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;

    protected $table = 'mahasiswa';
    protected $fillable = [
        'user_id',
        'nama',
        'nim',
        'tempat_lahir',
        'tanggal_lahir',
        'kelas',
        'program_studi',
        'nomor_ijazah',
        'tanggal_kelulusan'
    ];
}
