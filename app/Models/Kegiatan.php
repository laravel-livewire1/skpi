<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    use HasFactory;

    protected $table = 'kegiatan';
    protected $fillable = [
        'mahasiswa_id',
        'judul',
        'judul_english',
        'tingkat',
        'partisipasi',
        'penyelenggara',
        'tahun',
        'file',
        'url',
        'status_pengajuan',
        'status_validasi',
        'komentar',
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class);
    }
}
