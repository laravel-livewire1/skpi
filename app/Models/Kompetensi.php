<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kompetensi extends Model
{
    use HasFactory;

    protected $table = 'kompetensi';
    protected $fillable = [
        'mahasiswa_id',
        'judul',
        'judul_english',
        'bidang',
        'penyelenggara',
        'tanggal_kelulusan',
        'file',
        'url',
        'status_pengajuan',
        'status_validasi',
        'komentar'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class);
    }
}
