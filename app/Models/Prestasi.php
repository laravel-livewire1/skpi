<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prestasi extends Model
{
    use HasFactory;

    protected $table = 'prestasi';
    protected $fillable = [
        'mahasiswa_id',
        'judul',
        'judul_english',
        'tingkat',
        'peringkat',
        'penyelenggara',
        'tahun',
        'file',
        'url',
        'status_pengajuan',
        'status_validasi',
        'komentar',
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class);
    }
}
