<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\Kompetensi;
use App\Models\Mahasiswa;
use App\Models\Prestasi;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CetakSKPIController extends Controller
{
    public function index()
    {
        $mahasiswa = Mahasiswa::where('user_id', Auth::user()->id)->first();
        return view('mahasiswa.cetak-skpi', \compact('mahasiswa'));
    }

    public function pdf()
    {
        $mahasiswa = Mahasiswa::where('user_id', Auth::user()->id)->first();
        $kegiatans = Kegiatan::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->get();
        $prestasis = Prestasi::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->get();
        $kompetensis = Kompetensi::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->get();
        $pdf = Pdf::loadView('mahasiswa.cetak-skpi-pdf', \compact('mahasiswa', 'kegiatans', 'prestasis', 'kompetensis'))->setPaper('a4');
        return $pdf->stream();
    }
}
