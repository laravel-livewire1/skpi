<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\Kompetensi;
use App\Models\Mahasiswa;
use App\Models\Prestasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    public function index()
    {
        return view('mahasiswa.home');
    }

    public function daftar_pengajuan()
    {
        $mahasiswa = Mahasiswa::where('user_id', Auth::user()->id)->first();
        //count pengajuan
        $countKegiatan = Kegiatan::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->count();
        $countPrestasi = Prestasi::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->count();
        $countKompetensi = Kompetensi::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->count();
        $count = $countKegiatan + $countPrestasi + $countKompetensi;
        //looping data pengajuan
        $kegiatans = Kegiatan::where('mahasiswa_id', $mahasiswa->id)->orderByRaw("status_validasi = 'Tervalidasi' DESC")->orderBy('created_at', 'DESC')->get();
        $prestasis = Prestasi::where('mahasiswa_id', $mahasiswa->id)->orderByRaw("status_validasi = 'Tervalidasi' DESC")->orderBy('created_at', 'DESC')->get();
        $kompetensis = Kompetensi::where('mahasiswa_id', $mahasiswa->id)->orderByRaw("status_validasi = 'Tervalidasi' DESC")->orderBy('created_at', 'DESC')->get();
        return view('mahasiswa.daftar-pengajuan', \compact('mahasiswa', 'kegiatans', 'prestasis', 'kompetensis', 'count'));
    }

    public function pengajuan_skpi()
    {
        return view('mahasiswa.pengajuan-skpi');
    }

    public function store_pengajuan_kegiatan(Request $request)
    {
        $mahasiswa = Mahasiswa::where('user_id', Auth::user()->id)->first();
        $request->validate([
            'judul' => 'required',
            'tingkat' => 'required',
            'partisipasi' => 'required',
            'penyelenggara' => 'required',
            'tahun' => 'required',
            'file' => 'required|mimes:jpg,jpeg,png,pdf|max:1024'
        ]);

        $data = $request->all();
        $data['mahasiswa_id'] = $mahasiswa->id;
        $data['status_pengajuan'] = 'Pengajuan Baru';
        $data['status_validasi'] = 'Menunggu Validasi';
        try {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $fileName = time() . '_' . $file->getClientOriginalName();
                $filePath = $file->storeAs('mahasiswa/upload/kegiatan', $fileName);
                $data['file'] = $filePath;
            }
            //\dd($data);
            Kegiatan::create($data);
            Alert::toast('Data kegiatan berhasil diajukan!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::toast('Something wrong!', 'error');
            return redirect()->back();
        }
    }
}
