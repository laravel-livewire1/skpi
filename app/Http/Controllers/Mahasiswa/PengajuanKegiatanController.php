<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class PengajuanKegiatanController extends Controller
{
    public function store(Request $request)
    {
        $mahasiswa = Mahasiswa::where('user_id', Auth::user()->id)->first();
        $request->validate([
            'judul' => 'required',
            'judul_english' => 'required',
            'tingkat' => 'required',
            'partisipasi' => 'required',
            'penyelenggara' => 'required',
            'tahun' => 'required',
            'file' => 'required|mimes:jpg,jpeg,png,pdf|max:1024'
        ]);

        $data = $request->all();
        $data['mahasiswa_id'] = $mahasiswa->id;
        $data['status_pengajuan'] = 'Pengajuan Baru';
        $data['status_validasi'] = 'Menunggu Validasi';
        try {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $filePath = $file->store('mahasiswa/kegiatan', 'public');
                $data['file'] = $filePath;
            }
            //\dd($data);
            Kegiatan::create($data);
            Alert::toast('Data kegiatan berhasil diajukan!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::toast($th->getMessage(), 'error');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $kegiatan = Kegiatan::findOrFail($id);
        return view('mahasiswa.edit-pengajuan-kegiatan', \compact('kegiatan'));
    }

    public function update(Request $request, $id)
    {
        $kegiatan = Kegiatan::findOrFail($id);
        $request->validate([
            'judul' => 'required',
            'judul_english' => 'required',
            'tingkat' => 'required',
            'partisipasi' => 'required',
            'penyelenggara' => 'required',
            'tahun' => 'required',
            'file' => 'mimes:jpg,jpeg,png,pdf|max:1024'
        ]);

        $data = $request->all();
        $data['mahasiswa_id'] = $kegiatan->mahasiswa_id;
        if ($kegiatan->status_validasi == 'Ditolak') {
            $data['status_pengajuan'] = 'Revisi Pengajuan';
            $data['status_validasi'] = 'Menunggu Validasi';
            $data['komentar'] = null;
        } else {
            $data['status_pengajuan'] = $kegiatan->status_pengajuan;
            $data['status_validasi'] = $kegiatan->status_validasi;
        }
        try {
            if ($request->hasFile('file')) {
                Storage::delete('public/' . $kegiatan->file);
                $file = $request->file('file');
                $filePath = $file->store('mahasiswa/kegiatan', 'public');
                $data['file'] = $filePath;
            } else {
                $data['file'] = $kegiatan->file;
            }
            //\dd($data);
            $kegiatan->update($data);
            Alert::toast('Data berhasil diperbarui!', 'success');
            return redirect()->route('daftar-pengajuan');
        } catch (\Throwable $th) {
            Alert::toast($th->getMessage(), 'error');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        $kegiatan = Kegiatan::findOrFail($id);
        Storage::delete('public/' . $kegiatan->file);
        $kegiatan->delete();
        Alert::toast('Data kegiatan berhasil dihapus!', 'success');
        return redirect()->back();
    }
}
