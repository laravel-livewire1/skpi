<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use App\Models\Kompetensi;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class PengajuanKompetensiController extends Controller
{
    public function store(Request $request)
    {
        $mahasiswa = Mahasiswa::where('user_id', Auth::user()->id)->first();
        $request->validate([
            'judul' => 'required',
            'judul_english' => 'required',
            'bidang' => 'required',
            'penyelenggara' => 'required',
            'tanggal_kelulusan' => 'required|date',
            'file' => 'required|mimes:jpg,jpeg,png,pdf|max:1024'
        ]);

        $data = $request->all();
        $data['mahasiswa_id'] = $mahasiswa->id;
        $data['status_pengajuan'] = 'Pengajuan Baru';
        $data['status_validasi'] = 'Menunggu Validasi';
        try {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $filePath = $file->store('mahasiswa/kompetensi', 'public');
                $data['file'] = $filePath;
            }
            //\dd($data);
            Kompetensi::create($data);
            Alert::toast('Data kompetensi berhasil diajukan!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::toast($th->getMessage(), 'error');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $kompetensi = Kompetensi::findOrFail($id);
        return view('mahasiswa.edit-pengajuan-kompetensi', \compact('kompetensi'));
    }

    public function update(Request $request, $id)
    {
        $kompetensi = Kompetensi::findOrFail($id);
        $request->validate([
            'judul' => 'required',
            'judul_english' => 'required',
            'bidang' => 'required',
            'penyelenggara' => 'required',
            'tanggal_kelulusan' => 'required|date',
            'file' => 'mimes:jpg,jpeg,png,pdf|max:1024'
        ]);

        $data = $request->all();
        $data['mahasiswa_id'] = $kompetensi->mahasiswa_id;
        if ($kompetensi->status_validasi == 'Ditolak') {
            $data['status_pengajuan'] = 'Revisi Pengajuan';
            $data['status_validasi'] = 'Menunggu Validasi';
            $data['komentar'] = null;
        } else {
            $data['status_pengajuan'] = $kompetensi->status_pengajuan;
            $data['status_validasi'] = $kompetensi->status_validasi;
        }
        try {
            if ($request->hasFile('file')) {
                Storage::delete('public/' . $kompetensi->file);
                $file = $request->file('file');
                $filePath = $file->store('mahasiswa/kompetensi', 'public');
                $data['file'] = $filePath;
            } else {
                $data['file'] = $kompetensi->file;
            }
            //\dd($data);
            $kompetensi->update($data);
            Alert::toast('Data berhasil diperbarui!', 'success');
            return redirect()->route('daftar-pengajuan');
        } catch (\Throwable $th) {
            Alert::toast($th->getMessage(), 'error');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        $kompetensi = Kompetensi::findOrFail($id);
        Storage::delete('public/' . $kompetensi->file);
        $kompetensi->delete();
        Alert::toast('Data komptensi berhasil dihapus!', 'success');
        return redirect()->back();
    }
}
