<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\MahasiswaImport;
use App\Models\Kegiatan;
use App\Models\Kompetensi;
use App\Models\Mahasiswa;
use App\Models\Prestasi;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Validators\ValidationException;
use RealRashid\SweetAlert\Facades\Alert;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $mahasiswas = Mahasiswa::orderBy('nama', 'ASC')->get();
        return view('admin.mahasiswa.index', compact('mahasiswas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.mahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'nim' => 'required|unique:mahasiswa',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'kelas' => 'required',
            'program_studi' => 'required',
            'nomor_ijazah' => 'nullable|unique:mahasiswa',
            'tanggal_kelulusan' => 'nullable|date',
            'email' => 'required|email|unique:users'
        ]);

        $emailExists = User::where('email', $request->input('email'))->exists();
        $nimExists = Mahasiswa::where('nim', $request->input('nim'))->exists();

        try {
            if (!$emailExists && !$nimExists) {
                $user = User::create([
                    'name' => $request->input('nama'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('nim')),
                ]);

                Mahasiswa::create([
                    'user_id' => $user->id,
                    'nama' => $request->input('nama'),
                    'nim' => $request->input('nim'),
                    'tempat_lahir' => $request->input('tempat_lahir'),
                    'tanggal_lahir' => $request->input('tanggal_lahir'),
                    'kelas' => $request->input('kelas'),
                    'program_studi' => $request->input('program_studi'),
                    'nomor_ijazah' => $request->input('nomor_ijazah'),
                    'tanggal_kelulusan' => $request->input('tanggal_kelulusan')
                ]);
                Alert::toast('Data mahasiswa berhasil ditambahkan!', 'success');
                return redirect()->back();
            } else {
                Alert::toast('Email atau NIM sudah ada dalam database!', 'error');
                return redirect()->back();
            }
        } catch (\Throwable $th) {
            Alert::toast($th->getMessage(), 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);
        //count pengajuan
        $countKegiatan = Kegiatan::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->count();
        $countPrestasi = Prestasi::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->count();
        $countKompetensi = Kompetensi::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->count();
        $count = $countKegiatan + $countPrestasi + $countKompetensi;
        //looping data pengajuan
        $kegiatans = Kegiatan::where('mahasiswa_id', $mahasiswa->id)->orderByRaw("status_validasi = 'Tervalidasi' DESC")->orderBy('created_at', 'DESC')->get();
        $prestasis = Prestasi::where('mahasiswa_id', $mahasiswa->id)->orderByRaw("status_validasi = 'Tervalidasi' DESC")->orderBy('created_at', 'DESC')->get();
        $kompetensis = Kompetensi::where('mahasiswa_id', $mahasiswa->id)->orderByRaw("status_validasi = 'Tervalidasi' DESC")->orderBy('created_at', 'DESC')->get();
        return view('admin.mahasiswa.show', \compact('mahasiswa', 'kegiatans', 'prestasis', 'kompetensis', 'count'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);
        return view('admin.mahasiswa.edit', compact('mahasiswa'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);
        $user = User::where('id', $mahasiswa->user_id)->first();

        $request->validate([
            'nama' => 'required',
            'nim' => 'required|unique:mahasiswa,nim,' . $mahasiswa->id,
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'kelas' => 'required',
            'program_studi' => 'required',
            'nomor_ijazah' => 'nullable|unique:mahasiswa,nomor_ijazah,' . $mahasiswa->id,
            'tanggal_kelulusan' => 'nullable|date',
        ]);

        try {
            $user->update([
                'name' => $request->input('nama'),
            ]);

            $mahasiswa->update([
                'nama' => $request->input('nama'),
                'nim' => $request->input('nim'),
                'tempat_lahir' => $request->input('tempat_lahir'),
                'tanggal_lahir' => $request->input('tanggal_lahir'),
                'kelas' => $request->input('kelas'),
                'program_studi' => $request->input('program_studi'),
                'nomor_ijazah' => $request->input('nomor_ijazah'),
                'tanggal_kelulusan' => $request->input('tanggal_kelulusan')
            ]);
            Alert::toast('Data mahasiswa berhasil diperbarui!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::toast($th->getMessage(), 'error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);
        $user = User::where('id', $mahasiswa->user_id)->first();

        $mahasiswa->delete();
        $user->delete();
        Alert::toast('Data mahasiswa berhasil dihapus!', 'success');
        return redirect()->back();
    }

    public function import(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        DB::beginTransaction();
        try {
            Excel::import(new MahasiswaImport, $request->file('file'));
            DB::commit();
            Alert::toast('Import data mahasiswa berhasil!', 'success');
            return redirect()->back();
        } catch (ValidationException $e) {
            DB::rollBack();
            Alert::toast($e->failures(), 'error');
            return redirect()->back();
        }
    }

    public function cetak_skpi($id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);
        $kegiatans = Kegiatan::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->get();
        $prestasis = Prestasi::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->get();
        $kompetensis = Kompetensi::where('mahasiswa_id', $mahasiswa->id)->where('status_validasi', 'Tervalidasi')->get();
        $pdf = Pdf::loadView('admin.mahasiswa.cetak-skpi-pdf', \compact('mahasiswa', 'kegiatans', 'prestasis', 'kompetensis'))->setPaper('a4');
        return $pdf->stream();
    }
}
