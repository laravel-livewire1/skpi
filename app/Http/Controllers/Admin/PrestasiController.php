<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Prestasi;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PrestasiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $prestasis = Prestasi::orderByRaw("status_validasi = 'Tervalidasi' ASC")->orderBy('created_at', 'DESC')->get();
        return view('admin.prestasi.index', \compact('prestasis'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $prestasi = Prestasi::findOrFail($id);
        return view('admin.prestasi.edit', \compact('prestasi'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $prestasi = Prestasi::findOrFail($id);
        $data = $request->all();
        try {
            //\dd($data);
            $prestasi->update($data);
            Alert::toast('Data berhasil diperbarui!', 'success');
            return redirect()->route('prestasi.index');
        } catch (\Throwable $th) {
            Alert::toast($th->getMessage(), 'error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
