<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\Kompetensi;
use App\Models\Mahasiswa;
use App\Models\Prestasi;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $mahasiswa = Mahasiswa::count();
        $kegiatan = Kegiatan::count();
        $prestasi = Prestasi::count();
        $kompetensi = Kompetensi::count();
        return view('admin.dashboard', \compact('mahasiswa', 'kegiatan', 'prestasi', 'kompetensi'));
    }
}
