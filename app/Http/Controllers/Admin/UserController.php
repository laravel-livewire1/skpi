<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Models\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);

        $data = $request->all();
        $data['role'] = 'admin';
        $data['password'] = bcrypt($request->input('password'));
        try {
            User::create($data);
            Alert::toast('Data perngguna berhasil ditambahkan!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::toast($th->getMessage(), 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = User::findOrFail($id);
        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $user = User::findOrFail($id);
        $mahasiswa = Mahasiswa::where('user_id', $user->id)->first();
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'password' => 'nullable|min:8',
            'status' => 'required'
        ]);

        $data = $request->all();

        try {
            if ($request->input('password')) {
                $data['password'] = \bcrypt($request->input('password'));
            } else {
                $data['password'] = $user->password;
            }
            $user->update($data);
            $mahasiswa->update([
                'nama' => $request->input('name')
            ]);
            Alert::toast('Data perngguna berhasil diperbarui!', 'success');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::toast($th->getMessage(), 'error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        Alert::toast('Data pengguna berhasil dihapus!', 'success');
        return redirect()->back();
    }
}
