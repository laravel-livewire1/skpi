<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if ($user->role == 'admin' && $user->status == 1) {
                return redirect('admin/dashboard');
            } elseif ($user->role == 'mahasiswa' && $user->status == 1) {
                return redirect('mahasiswa/home');
            } else {
                Auth::logout();
                Alert::error('Login error!', 'Akun anda tidak aktif. Silahkan hubungi admin');
                return redirect()->back();
            }
        } else {
            Alert::error('Login error!', 'Email atau Password anda salah');
            return redirect()->back();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
