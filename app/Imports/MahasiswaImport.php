<?php

namespace App\Imports;

use App\Models\Mahasiswa;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class MahasiswaImport implements ToModel, WithHeadingRow, SkipsOnError, SkipsOnFailure, WithValidation
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $user = User::create([
            'name' => $row['nama'],
            'email' => $row['email'],
            'password' => Hash::make($row['nim'])
        ]);
        $mahasiswa = Mahasiswa::create([
            'user_id' => $user->id,
            'nama' => $row['nama'],
            'nim' => $row['nim'],
            'tempat_lahir' => $row['tempat_lahir'],
            'tanggal_lahir' => Date::excelToDateTimeObject($row['tanggal_lahir']),
            'kelas' => $row['kelas'],
            'program_studi' => $row['program_studi'],
            'nomor_ijazah' => $row['nomor_ijazah'],
            'tanggal_kelulusan' => $row['tanggal_kelulusan'],
        ]);
        return $mahasiswa;
    }

    public function rules(): array
    {
        return [
            'nim' => 'unique:mahasiswa,nim',
            'nomor_ijazah' => 'nullable|unique:mahasiswa,nomor_ijazah',
            'email' => 'unique:users,email'
        ];
    }

    public function onFailure(Failure ...$failures)
    {
    }
}
