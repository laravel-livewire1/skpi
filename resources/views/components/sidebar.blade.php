<div class="sidebar-menu">
    @if (Auth::user()->role == 'admin' && Auth::user()->status == 1)
    <ul class="menu">
        <li class="sidebar-title">Menu</li>
        <x-side-link :active="request()->routeIs('admin.dashboard')">
            <a href="{{ route('admin.dashboard') }}" class="sidebar-link">
                <i class="bi bi-grid-fill"></i>
                <span>Dashboard</span>
            </a>
        </x-side-link>
        <x-side-link
            class="sidebar-item has-sub {{ request()->routeIs('kegiatan.*') ? 'active':'' }} {{ request()->routeIs('prestasi.*') ? 'active':'' }} {{ request()->routeIs('kompetensi.*') ? 'active':'' }}">
            <a href="#" class="sidebar-link">
                <i class="bi bi-clipboard-fill"></i>
                <span>Pengajuan SKPI</span>
            </a>
            <ul
                class="submenu {{ request()->routeIs('kegiatan.*') ? 'active':'' }} {{ request()->routeIs('prestasi.*') ? 'active':'' }} {{ request()->routeIs('kompetensi.*') ? 'active':'' }}">
                <li class="submenu-item {{ request()->routeIs('kegiatan.*') ? 'active':'' }}">
                    <a href=" {{ route('kegiatan.index') }}" class="submenu-link">Kegiatan</a>
                </li>
                <li class="submenu-item {{ request()->routeIs('prestasi.*') ? 'active':'' }}">
                    <a href="{{ route('prestasi.index') }}" class="submenu-link">Prestasi</a>
                </li>
                <li class="submenu-item {{ request()->routeIs('kompetensi.*') ? 'active':'' }}">
                    <a href="{{ route('kompetensi.index') }}" class="submenu-link">Kompetensi</a>
                </li>
            </ul>
        </x-side-link>
        <x-side-link :active="request()->routeIs('mahasiswa.*')">
            <a href="{{ route('mahasiswa.index') }}" class="sidebar-link">
                <i class="bi bi-people-fill"></i>
                <span>Mahasiswa</span>
            </a>
        </x-side-link>
        <x-side-link :active="request()->routeIs('user.*')">
            <a href="{{ route('user.index') }}" class="sidebar-link">
                <i class="bi bi-person-check-fill"></i>
                <span>Pengguna</span>
            </a>
        </x-side-link>
        <x-side-link :active="request()->routeIs('logout')">
            <a href="{{ route('logout') }}" class="sidebar-link"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="bi bi-box-arrow-right"></i>
                <span>Sign Out</span>
                <form id="logout-form" method="POST" action="{{ route('logout') }}" style="display: none;">
                    @csrf
                </form>
            </a>
        </x-side-link>
    </ul>
    @elseif (Auth::user()->role == 'mahasiswa' && Auth::user()->status == 1)
    <ul class="menu">
        <li class="sidebar-title">Menu</li>
        <x-side-link :active="request()->routeIs('home')">
            <a href="{{ route('home') }}" class="sidebar-link">
                <i class="bi bi-grid-fill"></i>
                <span>Dashboard</span>
            </a>
        </x-side-link>
        <x-side-link :active="request()->routeIs('daftar-pengajuan')">
            <a href="{{ route('daftar-pengajuan') }}" class="sidebar-link">
                <i class="bi bi-hdd-stack-fill"></i>
                <span>Daftar Pengajuan</span>
            </a>
        </x-side-link>
        <x-side-link :active="request()->routeIs('pengajuan-skpi')">
            <a href="{{ route('pengajuan-skpi') }}" class="sidebar-link">
                <i class="bi bi-clipboard-fill"></i>
                <span>Pengajuan SKPI</span>
            </a>
        </x-side-link>
        <x-side-link :active="request()->routeIs('cetak-skpi')">
            <a href="{{ route('cetak-skpi') }}" class="sidebar-link">
                <i class="bi bi-printer-fill"></i>
                <span>Cetak SKPI</span>
            </a>
        </x-side-link>
        <x-side-link>
            <a href="{{ route('logout') }}" class="sidebar-link"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="bi bi-box-arrow-right"></i>
                <span>Sign Out</span>
                <form id="logout-form" method="POST" action="{{ route('logout') }}" style="display: none;">
                    @csrf
                </form>
            </a>
        </x-side-link>
    </ul>
    @endif
</div>
