<x-app title="Pengajuan SKPI">
    <div class="page-heading">
        <h3>Pengajuan SKPI</h3>
    </div>
    <div class="page-content">
        <div class="alert alert-light-info color-info" style="font-size: 14px;">
            <span><b>Judul Kegiatan, Prestasi dan Komptensi</b> yang diketikkan oleh mahasiswa adalah yang akan
                ditampilkan pada cetakan
                transkrip SKPI (Surat Keterangan Pendamping Ijazah), oleh karena itu diharapkan judul diketik tidak
                terlalu panjang dan menggunakan ejaan kata yang benar</span>
        </div>
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="kegiatan-tab" data-bs-toggle="tab" href="#kegiatan" role="tab"
                            aria-controls="kegiatan" aria-selected="true">Kegiatan</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="prestasi-tab" data-bs-toggle="tab" href="#prestasi" role="tab"
                            aria-controls="prestasi" aria-selected="false">Prestasi</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="kompetensi-tab" data-bs-toggle="tab" href="#kompetensi" role="tab"
                            aria-controls="kompetensi" aria-selected="false">Kompetensi</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="kegiatan" role="tabpanel" aria-labelledby="kegiatan-tab">
                        <form action="{{ route('pengajuan-kegiatan.store') }}" method="POST" class="mt-4"
                            enctype="multipart/form-data">
                            @csrf
                            <label class="badge bg-primary p-2" style="font-size: 16px;">Form Pengajuan Kegiatan</label>
                            <div class="row mt-4">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Judul Kegiatan <span class="text-danger">*</span></label>
                                        <input type="text" name="judul" class="form-control @error('judul')
                                            is-invalid
                                        @enderror" value="{{ old('judul') }}" placeholder="cth: Seminar Nasional"
                                            required>
                                        @error('judul')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Judul Kegiatan (English)<span class="text-danger">*</span></label>
                                        <input type="text" name="judul_english" class="form-control @error('judul_english')
                                            is-invalid
                                        @enderror" value="{{ old('judul_english') }}"
                                            placeholder="cth: International Workshop" required>
                                        @error('judul_english')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Tingkat <span class="text-danger">*</span></label>
                                        <select name="tingkat" class="form-control @error('tingkat')
                                            is-invalid
                                        @enderror" required>
                                            <option value="">-- Pilih Tingkat --</option>
                                            <option value="Internasional">Internasional</option>
                                            <option value="Nasional">Nasional</option>
                                            <option value="Wilayah">Wilayah</option>
                                        </select>
                                        @error('tingkat')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Partisipasi <span class="text-danger">*</span></label>
                                        <select name="partisipasi" class="form-control @error('partisipasi')
                                            is-invalid
                                        @enderror" required>
                                            <option value="">-- Pilih Partisipasi --</option>
                                            <option value="Ketua">Ketua</option>
                                            <option value="Anggota">Anggota</option>
                                            <option value="Peserta">Peserta</option>
                                        </select>
                                        @error('partisipasi')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Penyelenggara <span class="text-danger">*</span></label>
                                        <input type="text" name="penyelenggara" class="form-control @error('penyelenggara')
                                            is-invalid
                                        @enderror" value="{{ old('penyelenggara') }}"
                                            placeholder="cth: STIE Cendekia Karya Utama" required>
                                        @error('penyelenggara')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Tahun <span class="text-danger">*</span></label>
                                        <input type="text" name="tahun" class="form-control @error('tahun')
                                            is-invalid
                                        @enderror" value="{{ old('tahun') }}" required>
                                        @error('tahun')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>File <span class="text-danger">*</span></label>
                                        <input type="file" name="file" class="form-control @error('file')
                                            is-invalid
                                        @enderror" value="{{ old('file') }}" required>
                                        @error('file')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>URL Kegiatan <span>(Opsional)</span></label>
                                        <input type="text" name="url" class="form-control" value="{{ old('url') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="float-end">
                                <button type="reset" class="btn btn-sm btn-light"><i class="bi bi-x"></i> Reset</button>
                                <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-send-fill"></i>
                                    Ajukan</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="prestasi" role="tabpanel" aria-labelledby="prestasi-tab">
                        <form action="{{ route('pengajuan-prestasi.store') }}" method="POST" class="mt-4"
                            enctype="multipart/form-data">
                            @csrf
                            <label class="badge bg-primary p-2" style="font-size: 16px;">Form Pengajuan Prestasi</label>
                            <div class="row mt-4">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Judul Prestasi <span class="text-danger">*</span></label>
                                        <input type="text" name="judul" class="form-control @error('judul')
                                            is-invalid
                                        @enderror" value="{{ old('judul') }}" placeholder="cth: Cerdas Cermat"
                                            required>
                                        @error('judul')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Judul Prestasi (English)<span class="text-danger">*</span></label>
                                        <input type="text" name="judul_english" class="form-control @error('judul_english')
                                            is-invalid
                                        @enderror" value="{{ old('judul_english') }}"
                                            placeholder="cth: International Robotics Students" required>
                                        @error('judul_english')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Tingkat <span class="text-danger">*</span></label>
                                        <select name="tingkat" class="form-control @error('tingkat')
                                            is-invalid
                                        @enderror" required>
                                            <option value="">-- Pilih Tingkat --</option>
                                            <option value="Internasional">Internasional</option>
                                            <option value="Nasional">Nasional</option>
                                            <option value="Wilayah">Wilayah</option>
                                        </select>
                                        @error('tingkat')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Peringkat <span class="text-danger">*</span></label>
                                        <select name="peringkat" class="form-control @error('peringkat')
                                            is-invalid
                                        @enderror" required>
                                            <option value="">-- Pilih Peringkat --</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                        @error('peringkat')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Penyelenggara <span class="text-danger">*</span></label>
                                        <input type="text" name="penyelenggara" class="form-control @error('penyelenggara')
                                            is-invalid
                                        @enderror" value="{{ old('penyelenggara') }}"
                                            placeholder="cth: STIE Cendekia Karya Utama" required>
                                        @error('penyelenggara')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Tahun <span class="text-danger">*</span></label>
                                        <input type="text" name="tahun" class="form-control @error('tahun')
                                            is-invalid
                                        @enderror" value="{{ old('tahun') }}" required>
                                        @error('tahun')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>File <span class="text-danger">*</span></label>
                                        <input type="file" name="file" class="form-control @error('file')
                                            is-invalid
                                        @enderror" required>
                                        @error('file')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>URL Kegiatan <span>(Opsional)</span></label>
                                        <input type="text" name="url" class="form-control" value="{{ old('url') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="float-end">
                                <button type="reset" class="btn btn-sm btn-light"><i class="bi bi-x"></i> Reset</button>
                                <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-send-fill"></i>
                                    Ajukan</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="kompetensi" role="tabpanel" aria-labelledby="kompetensi-tab">
                        <form action="{{ route('pengajuan-kompetensi.store') }}" method="POST" class="mt-4"
                            enctype="multipart/form-data">
                            @csrf
                            <label class="badge bg-primary p-2" style="font-size: 16px;">Form Pengajuan
                                Kompetensi</label>
                            <div class="row mt-4">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Judul Kompetensi <span class="text-danger">*</span></label>
                                        <input type="text" name="judul" class="form-control @error('judul')
                                            is-invalid
                                        @enderror" value="{{ old('judul') }}" placeholder="cth: Seminar Nasional"
                                            required>
                                        @error('judul')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Judul Kompetensi (English)<span class="text-danger">*</span></label>
                                        <input type="text" name="judul_english" class="form-control @error('judul_english')
                                            is-invalid
                                        @enderror" value="{{ old('judul_english') }}"
                                            placeholder="cth: International Competition" required>
                                        @error('judul_english')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Bidang <span class="text-danger">*</span></label>
                                        <input type="text" name="bidang" class="form-control @error('bidang')
                                            is-invalid
                                        @enderror" value="{{ old('bidang') }}" placeholder="cth: Logistik" required>
                                        @error('bidang')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Penyelenggara <span class="text-danger">*</span></label>
                                        <input type="text" name="penyelenggara" class="form-control @error('penyelenggara')
                                            is-invalid
                                        @enderror" value="{{ old('penyelenggara') }}"
                                            placeholder="cth: STIE Cendekia Karya Utama" required>
                                        @error('penyelenggara')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Kelulusan <span class="text-danger">*</span></label>
                                        <input type="date" name="tanggal_kelulusan" class="form-control @error('tanggal_kelulusan')
                                            is-invalid
                                        @enderror" value="{{ old('tanggal_kelulusan') }}" required>
                                        @error('tanggal_kelulusan')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>File <span class="text-danger">*</span></label>
                                        <input type="file" name="file" class="form-control @error('file')
                                            is-invalid
                                        @enderror" value="{{ old('file') }}" required>
                                        @error('file')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>URL Kegiatan <span>(Opsional)</span></label>
                                        <input type="text" name="url" class="form-control" value="{{ old('url') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="float-end">
                                <button type="reset" class="btn btn-sm btn-light"><i class="bi bi-x"></i> Reset</button>
                                <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-send-fill"></i>
                                    Ajukan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app>