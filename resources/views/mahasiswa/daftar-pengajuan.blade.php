<x-app title="Daftar Pengajuan">
    <div class="page-heading">
        <h3>Daftar Pengajuan</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Informasi</h5>
                <hr>
            </div>
            <div class="card-body" style="font-size: 14px;">
                <ol style="margin-top: -30px;">
                    <li>Arti dari status ajuan berikut :</li>
                    <ul>
                        <li><b>Pengajuan Baru</b>, mahasiswa baru saja melakukan pengajuan SKPI dan menunggu pengajuan
                            diverifikasi admin baak;</li>
                        <li><b>Revisi Pengajuan</b>, pengajuan SKPI dikembalikan dan harus direvisi;</li>
                        <li><b>Menunggu Validasi</b>, mahasiswa baru saja melakukan pengajuan/revisi dan menunggu
                            pengajuan diverifikasi dan divalidasi admin baak;</li>
                        <li><b>Tervalidasi</b>, pengajuan SKPI diterima dan sudah divalidasi oleh admin baak;</li>
                        <li><b>Ditolak</b>, pengajuan SKPI ditolak (perhatikan komentar nya)</li>
                    </ul>
                    <li>Pada <b>Timeline Ajuan</b> (2 dots), mahasiswa hanya dapat melakukan
                        edit ajuan apabila :</li>
                    <ul>
                        <li>Baru saja mengirim pengajuan baru;</li>
                        <li>pengajuan dikembalikan/ditolak;</li>
                        <li>Baru saja mengirim revisi ajuan</li>
                    </ul>
                    <li>Setelah mengirim revisi ajuan, status pada <b>Timeline Ajuan</b> akan menjadi <i
                            class="font-bold">Menunggu Validasi</i></li>
                    <li>Total SKP baru akan tampil apabila status ajuan sudah <i class="font-bold">diterima</i> oleh
                        admin baak</li>
                    <li><b>Judul</b> yang dituliskan oleh mahasiswa adalah yang akan ditampilkan pada cetakan
                        transkrip SKPI (Surat Keterangan Pendamping Ijazah), oleh karena itu diharapkan judul diketik
                        tidak terlalu panjang dan menggunakan ejaan kata yang benar</li>
                </ol>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="m-v-xs">
                            <label class="font-bold text-lg">{{ $mahasiswa->nim }}</label>
                            <br><label class="">{{ strtoupper($mahasiswa->nama) }}</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="m-v-xs">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="m-v-xs text-center">
                            <label class="text-muted">Total SKP tervalidasi :</label>
                            <br><label class="font-bold text-lg">{{ $count }}</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <i class="bi bi-clipboard-fill"></i><span class="text-dark" style="font-size: 14px;"> Daftar
                    Pengajuan</span>
            </div>
            <div class="card-body">
                <p class="bg-primary text-white text-center p-2" style="font-size: 14px;font-weight: bold;">Kegiatan</p>

                @foreach ($kegiatans as $kegiatan)
                <div class="card border-primary mb-2 shadow-sm">
                    <div class="card-body">
                        <div class="status">
                            @if ($kegiatan->status_validasi == 'Tervalidasi')
                            <span class="badge bg-success p-2"><i class="bi bi-check-circle-fill"></i> {{
                                $kegiatan->status_validasi
                                }}</span>
                            <span class="badge bg-success p-2"><i class="bi bi-check-circle-fill"></i> {{
                                \Carbon\Carbon::parse($kegiatan->updated_at)->isoFormat('D MMMM Y') }}</span>
                            @else
                            <span class="badge bg-info p-2"><i class="bi bi-send-fill"></i> {{
                                $kegiatan->status_pengajuan
                                }}</span>
                            @if ($kegiatan->status_validasi == 'Ditolak')
                            <span class="badge bg-danger p-2"><i class="bi bi-x"></i> {{
                                $kegiatan->status_validasi
                                }}</span>
                            @endif
                            <span class="badge bg-info p-2"><i class="bi bi-send-fill"></i> {{
                                \Carbon\Carbon::parse($kegiatan->updated_at)->isoFormat('D MMMM Y') }}</span>
                            @endif
                        </div>
                        <hr>
                        <div class="row" style="font-size: 14px;">
                            <div class="col-sm-8 col-md-10">
                                <div class="row mb-1">
                                    <div class="col-sm-3">Tanggal Pengajuan</div>
                                    <div class="col-sm-9">{{\Carbon\Carbon::parse($kegiatan->created_at)->isoFormat('D
                                        MMMM Y') }}</div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Judul Kegiatan</div>
                                    <div class="col-sm-9">
                                        <b>{{ $kegiatan->judul }}</b><br>
                                        <i style="font-size: 13px;">{{ $kegiatan->judul_english }}</i>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Tingkat</div>
                                    <div class="col-sm-9"><b>{{ strtoupper($kegiatan->tingkat) }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Partisipasi</div>
                                    <div class="col-sm-9"><b>{{ strtoupper($kegiatan->partisipasi) }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Penyelenggara</div>
                                    <div class="col-sm-9"><b>{{ strtoupper($kegiatan->penyelenggara) }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Tahun</div>
                                    <div class="col-sm-9"><b>{{ $kegiatan->tahun }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">File</div>
                                    <div class="col-sm-9">
                                        <a href="{{  Storage::url($kegiatan->file) }}" class="btn btn-sm btn-danger"
                                            target="_blank"><i class="bi bi-file-pdf-fill"></i>
                                            File / Dok</a>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">URL</div>
                                    <div class="col-sm-9"><b>{{ $kegiatan->url }}</b></div>
                                </div>
                                @if ($kegiatan->komentar != null)
                                <div class="row mb-1">
                                    <div class="col-sm-3">Komentar</div>
                                    <div class="col-sm-9">{{ $kegiatan->komentar }}</div>
                                </div>
                                @endif
                            </div>
                            <div class="col-sm-4 col-md-2">
                                @if ($kegiatan->status_validasi != 'Tervalidasi')
                                <a href="{{ route('pengajuan-kegiatan.edit',$kegiatan->id) }}"
                                    class="btn btn-sm btn-outline-primary"><i class="bi bi-pencil-fill"></i>
                                    Edit</a>
                                <form action="{{ route('pengajuan-kegiatan.destroy',$kegiatan->id) }}" class="d-inline"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-outline-danger"
                                        onclick="return confirm('Yakin ingin menghapus data ini ?')"><i
                                            class="bi bi-x"></i></button>
                                </form>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="margin-bottom: -25px;margin-top:-5px;">
                            <div class="col-sm-12">
                                <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                                    <div class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover"
                                            data-placement="top" title=""
                                            data-content="And here's some amazing content. It's very engaging. Right?"
                                            data-original-title="2003">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mb-1 font-bold" style="font-size: 13px;">1. Pengajuan</p>
                                            <p class="h6 text-muted mb-0 mb-lg-0" style="font-size: 13px;"></p>
                                        </div>
                                    </div>
                                    <div class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover"
                                            data-placement="top" title=""
                                            data-content="And here's some amazing content. It's very engaging. Right?"
                                            data-original-title="2004">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mb-1 font-bold" style="font-size: 13px;">2. Admin BAAK</p>
                                            @if ($kegiatan->status_validasi == 'Tervalidasi')
                                            <p class="text-success mb-0 mb-lg-0 ml-1" style="font-size: 13px;">Diterima
                                            </p>
                                            @else
                                            <p class="text-muted mb-0 mb-lg-0" style="font-size: 13px;">Menunggu
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                <p class="bg-primary text-white text-center p-2 mt-3" style="font-size: 14px; font-weight: bold;">
                    Prestasi
                </p>
                @foreach ($prestasis as $prestasi)
                <div class="card border-primary mb-2 shadow-sm">
                    <div class="card-body">
                        <div class="status">
                            @if ($prestasi->status_validasi == 'Tervalidasi')
                            <span class="badge bg-success p-2"><i class="bi bi-check-circle-fill"></i> {{
                                $prestasi->status_validasi
                                }}</span>
                            <span class="badge bg-success p-2"><i class="bi bi-check-circle-fill"></i> {{
                                \Carbon\Carbon::parse($prestasi->updated_at)->isoFormat('D MMMM Y') }}</span>
                            @else
                            <span class="badge bg-info p-2"><i class="bi bi-send-fill"></i> {{
                                $prestasi->status_pengajuan
                                }}</span>
                            @if ($prestasi->status_validasi == 'Ditolak')
                            <span class="badge bg-danger p-2"><i class="bi bi-x"></i> {{
                                $prestasi->status_validasi
                                }}</span>
                            @endif
                            <span class="badge bg-info p-2"><i class="bi bi-send-fill"></i> {{
                                \Carbon\Carbon::parse($prestasi->updated_at)->isoFormat('D MMMM Y') }}</span>
                            @endif
                        </div>
                        <hr>
                        <div class="row" style="font-size: 14px;">
                            <div class="col-sm-8 col-md-10">
                                <div class="row mb-1">
                                    <div class="col-sm-3">Tanggal Pengajuan</div>
                                    <div class="col-sm-9">{{\Carbon\Carbon::parse($prestasi->created_at)->isoFormat('D
                                        MMMM Y') }}</div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Judul Prestasi</div>
                                    <div class="col-sm-9">
                                        <b>{{ $prestasi->judul }}</b><br>
                                        <i style="font-size: 13px;">{{ $prestasi->judul_english }}</i>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Tingkat</div>
                                    <div class="col-sm-9"><b>{{ strtoupper($prestasi->tingkat) }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Peringkat</div>
                                    <div class="col-sm-9"><b>{{ strtoupper($prestasi->peringkat) }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Penyelenggara</div>
                                    <div class="col-sm-9"><b>{{ strtoupper($prestasi->penyelenggara) }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Tahun</div>
                                    <div class="col-sm-9"><b>{{ $prestasi->tahun }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">File</div>
                                    <div class="col-sm-9">
                                        <a href="{{  Storage::url($prestasi->file) }}" class="btn btn-sm btn-danger"
                                            target="_blank"><i class="bi bi-file-pdf-fill"></i>
                                            File / Dok</a>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">URL</div>
                                    <div class="col-sm-9"><b>{{ $prestasi->url }}</b></div>
                                </div>
                                @if ($prestasi->komentar != null)
                                <div class="row mb-1">
                                    <div class="col-sm-3">Komentar</div>
                                    <div class="col-sm-9">{{ $prestasi->komentar }}</div>
                                </div>
                                @endif
                            </div>
                            <div class="col-sm-4 col-md-2">
                                @if ($prestasi->status_validasi != 'Tervalidasi')
                                <a href="{{ route('pengajuan-prestasi.edit',$prestasi->id) }}"
                                    class="btn btn-sm btn-outline-primary"><i class="bi bi-pencil-fill"></i>
                                    Edit</a>
                                <form action="{{ route('pengajuan-prestasi.destroy',$prestasi->id) }}" class="d-inline"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-outline-danger"
                                        onclick="return confirm('Yakin ingin menghapus data ini ?')"><i
                                            class="bi bi-x"></i></button>
                                </form>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="margin-bottom: -25px;margin-top:-5px;">
                            <div class="col-sm-12">
                                <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                                    <div class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover"
                                            data-placement="top" title=""
                                            data-content="And here's some amazing content. It's very engaging. Right?"
                                            data-original-title="2003">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mb-1 font-bold" style="font-size: 13px;">1. Pengajuan</p>
                                            <p class="h6 text-muted mb-0 mb-lg-0" style="font-size: 13px;"></p>
                                        </div>
                                    </div>
                                    <div class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover"
                                            data-placement="top" title=""
                                            data-content="And here's some amazing content. It's very engaging. Right?"
                                            data-original-title="2004">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mb-1 font-bold" style="font-size: 13px;">2. Admin BAAK</p>
                                            @if ($prestasi->status_validasi == 'Tervalidasi')
                                            <p class="text-success mb-0 mb-lg-0 ml-1" style="font-size: 13px;">Diterima
                                            </p>
                                            @else
                                            <p class="text-muted mb-0 mb-lg-0" style="font-size: 13px;">Menunggu
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <p class="bg-primary text-white text-center p-2 mt-3" style="font-size: 14px; font-weight: bold;">
                    Kompetensi
                </p>
                @foreach ($kompetensis as $kompetensi)
                <div class="card border-primary mb-2 shadow-sm">
                    <div class="card-body">
                        <div class="status">
                            @if ($kompetensi->status_validasi == 'Tervalidasi')
                            <span class="badge bg-success p-2"><i class="bi bi-check-circle-fill"></i> {{
                                $kompetensi->status_validasi
                                }}</span>
                            <span class="badge bg-success p-2"><i class="bi bi-check-circle-fill"></i> {{
                                \Carbon\Carbon::parse($kompetensi->updated_at)->isoFormat('D MMMM Y') }}</span>
                            @else
                            <span class="badge bg-info p-2"><i class="bi bi-send-fill"></i> {{
                                $kompetensi->status_pengajuan
                                }}</span>
                            @if ($kompetensi->status_validasi == 'Ditolak')
                            <span class="badge bg-danger p-2"><i class="bi bi-x"></i> {{
                                $kompetensi->status_validasi
                                }}</span>
                            @endif
                            <span class="badge bg-info p-2"><i class="bi bi-send-fill"></i> {{
                                \Carbon\Carbon::parse($kompetensi->updated_at)->isoFormat('D MMMM Y') }}</span>
                            @endif
                        </div>
                        <hr>
                        <div class="row" style="font-size: 14px;">
                            <div class="col-sm-8 col-md-10">
                                <div class="row mb-1">
                                    <div class="col-sm-3">Tanggal Pengajuan</div>
                                    <div class="col-sm-9">{{\Carbon\Carbon::parse($kompetensi->created_at)->isoFormat('D
                                        MMMM Y') }}</div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Judul Kompetensi</div>
                                    <div class="col-sm-9">
                                        <b>{{ $kompetensi->judul }}</b><br>
                                        <i style="font-size: 13px;">{{ $kompetensi->judul_english }}</i>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Bidang</div>
                                    <div class="col-sm-9"><b>{{ strtoupper($kompetensi->bidang) }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Penyelenggara</div>
                                    <div class="col-sm-9"><b>{{ strtoupper($kompetensi->penyelenggara) }}</b></div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">Tanggal Kelulusan</div>
                                    <div class="col-sm-9">
                                        <b>{{\Carbon\Carbon::parse($kompetensi->tanggal_kelulusan)->isoFormat('D
                                            MMMM Y') }}</b>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">File</div>
                                    <div class="col-sm-9">
                                        <a href="{{  Storage::url($kompetensi->file) }}" class="btn btn-sm btn-danger"
                                            target="_blank"><i class="bi bi-file-pdf-fill"></i>
                                            File / Dok</a>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-sm-3">URL</div>
                                    <div class="col-sm-9"><b>{{ $kompetensi->url }}</b></div>
                                </div>
                                @if ($kompetensi->komentar != null)
                                <div class="row mb-1">
                                    <div class="col-sm-3">Komentar</div>
                                    <div class="col-sm-9">{{ $kompetensi->komentar }}</div>
                                </div>
                                @endif
                            </div>
                            <div class="col-sm-4 col-md-2">
                                @if ($kompetensi->status_validasi != 'Tervalidasi')
                                <a href="{{ route('pengajuan-kompetensi.edit',$kompetensi->id) }}"
                                    class="btn btn-sm btn-outline-primary"><i class="bi bi-pencil-fill"></i>
                                    Edit</a>
                                <form action="{{ route('pengajuan-kompetensi.destroy',$kompetensi->id) }}"
                                    class="d-inline" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-outline-danger"
                                        onclick="return confirm('Yakin ingin menghapus data ini ?')"><i
                                            class="bi bi-x"></i></button>
                                </form>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="margin-bottom: -25px;margin-top:-5px;">
                            <div class="col-sm-12">
                                <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                                    <div class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover"
                                            data-placement="top" title=""
                                            data-content="And here's some amazing content. It's very engaging. Right?"
                                            data-original-title="2003">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mb-1 font-bold" style="font-size: 13px;">1. Pengajuan</p>
                                            <p class="h6 text-muted mb-0 mb-lg-0" style="font-size: 13px;"></p>
                                        </div>
                                    </div>
                                    <div class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover"
                                            data-placement="top" title=""
                                            data-content="And here's some amazing content. It's very engaging. Right?"
                                            data-original-title="2004">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mb-1 font-bold" style="font-size: 13px;">2. Admin BAAK</p>
                                            @if ($kompetensi->status_validasi == 'Tervalidasi')
                                            <p class="text-success mb-0 mb-lg-0 ml-1" style="font-size: 13px;">Diterima
                                            </p>
                                            @else
                                            <p class="text-muted mb-0 mb-lg-0" style="font-size: 13px;">Menunggu
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</x-app>

<style>
    .timeline-steps {
        display: flex;
        justify-content: center;
        flex-wrap: wrap
    }

    .timeline-steps .timeline-step {
        align-items: center;
        display: flex;
        flex-direction: column;
        position: relative;
        margin: 1rem;
    }

    @media (min-width:768px) {
        .timeline-steps .timeline-step:not(:last-child):after {
            content: "";
            display: block;
            border-top: .15rem solid rgb(67, 94, 190);
            width: 4.46rem;
            position: absolute;
            left: 6.5rem;
            top: .3125rem
        }

        .timeline-steps .timeline-step:not(:first-child):before {
            content: "";
            display: block;
            border-top: .15rem solid rgb(67, 94, 190);
            width: 4.8125rem;
            position: absolute;
            right: 6.5rem;
            top: .3125rem
        }
    }

    .timeline-steps .timeline-content {
        width: 10rem;
        text-align: center
    }

    .timeline-steps .timeline-content .inner-circle {
        border-radius: 1.5rem;
        height: 0;
        width: 1rem;
        display: inline-flex;
        align-items: center;
        justify-content: center;
    }

    .timeline-steps .timeline-content .inner-circle:before {
        content: "";
        background-color: #ffffff;
        display: inline-block;
        height: 1.2rem;
        width: 1.2rem;
        min-width: 1.2rem;
        border-radius: 6.25rem;
        border: 0.2em solid rgb(25, 135, 84)
    }
</style>