<x-app title="Cetak SKPI">
    <div class="page-heading">
        <h3>Cetak SKPI</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="text-xs text-muted" style="font-size: 13px;">NIM</label><br>
                        <label class="text-md">{{ $mahasiswa->nim }}</label><br><br>
                        <label class="text-xs text-muted" style="font-size: 13px;">Nama Mahasiswa</label><br>
                        <label class="text-md">{{ strtoupper($mahasiswa->nama) }}</label><br><br>
                        <label class="text-xs text-muted" style="font-size: 13px;">Tempat/Tanggal Lahir</label><br>
                        <label class="text-md">{{ $mahasiswa->tempat_lahir }}, {{
                            \Carbon\Carbon::parse($mahasiswa->tanggal_lahir)->isoFormat('D MMMM Y') }}</label><br><br>
                        <label class="text-xs text-muted" style="font-size: 13px;">Kelas</label><br>
                        <label class="text-md">{{ $mahasiswa->kelas }}</label><br><br>
                    </div>
                    <div class="col-sm-6">
                        <label class="text-xs text-muted" style="font-size: 13px;">Program Studi</label><br>
                        <label class="text-md">{{ $mahasiswa->program_studi }}</label><br><br>
                        <label class="text-xs text-muted" style="font-size: 13px;">Nomor Ijazah</label><br>
                        <label class="text-md">
                            @if ($mahasiswa->nomor_ijazah)
                            {{ $mahasiswa->nomor_ijazah }}
                            @else
                            -
                            @endif
                        </label><br><br>
                        <label class="text-xs text-muted" style="font-size: 13px;">Tanggal Kelulusan</label><br>
                        <label class="text-md">
                            @if ($mahasiswa->tanggal_kelulusan)
                            {{ \Carbon\Carbon::parse($mahasiswa->tanggal_kelulusan)->isoFormat('D MMMM Y') }}
                            @else
                            -
                            @endif
                        </label><br><br>
                    </div>
                </div>
                @if ($mahasiswa->nomor_ijazah && $mahasiswa->tanggal_kelulusan)
                <a href="{{ route('cetak-skpi.pdf') }}" class="btn btn-sm btn-danger" target="_blank"><i
                        class="bi bi-file-pdf-fill"></i> Cetak SKPI</a>
                @else
                <div class="alert alert-warning">
                    <i class="fa fa-frown-o"></i> <span class="">Anda belum dapat mengakses menu ini jika nomor ijazah
                        dan tanggal kelulusan <b>belum diinputkan BAAK</b></span>
                </div>
                @endif
            </div>
        </div>
    </div>
</x-app>