<x-app title="Edit Pengajuan Kompetensi">
    <div class="page-heading">
        <h3>Edit Pengajuan Kompetensi</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('pengajuan-kompetensi.update',$kompetensi->id) }}" method="POST" class="mt-4"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <label class="badge bg-primary p-2" style="font-size: 16px;">Form Edit Pengajuan
                        Kompetensi</label>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Judul Kompetensi <span class="text-danger">*</span></label>
                                <input type="text" name="judul" class="form-control @error('judul')
                                            is-invalid
                                        @enderror" value="{{ $kompetensi->judul }}" placeholder="cth: Seminar Nasional"
                                    required>
                                @error('judul')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Judul Kompetensi (English)<span class="text-danger">*</span></label>
                                <input type="text" name="judul_english" class="form-control @error('judul_english')
                                            is-invalid
                                        @enderror" value="{{ $kompetensi->judul_english }}"
                                    placeholder="cth: Seminar Nasional" required>
                                @error('judul_english')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Bidang <span class="text-danger">*</span></label>
                                <input type="text" name="bidang" class="form-control @error('bidang')
                                            is-invalid
                                        @enderror" value="{{ $kompetensi->bidang }}" placeholder="cth: Logistik"
                                    required>
                                @error('bidang')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Penyelenggara <span class="text-danger">*</span></label>
                                <input type="text" name="penyelenggara" class="form-control @error('penyelenggara')
                                            is-invalid
                                        @enderror" value="{{ $kompetensi->penyelenggara }}"
                                    placeholder="cth: STIE Cendekia Karya Utama" required>
                                @error('penyelenggara')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Kelulusan <span class="text-danger">*</span></label>
                                <input type="date" name="tanggal_kelulusan" class="form-control @error('tanggal_kelulusan')
                                            is-invalid
                                        @enderror" value="{{ date($kompetensi->tanggal_kelulusan) }}" required>
                                @error('tanggal_kelulusan')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>File <span class="text-danger">*</span>( <a
                                        href="{{ Storage::url($kompetensi->file) }}" class="text-sm" target="_blank">
                                        Lihat
                                        File</a> )</label>
                                <input type="file" name="file" class="form-control @error('file')
                                            is-invalid
                                        @enderror">
                                @error('file')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>URL Kegiatan <span>(Opsional)</span></label>
                                <input type="text" name="url" class="form-control" value="{{ $kompetensi->url }}">
                            </div>
                        </div>
                    </div>
                    <div class="float-end">
                        <a href="{{ route('daftar-pengajuan') }}" class="btn btn-sm btn-light"><i
                                class="bi bi-arrow-left"></i> Back</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-send-fill"></i>
                            Ajukan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app>