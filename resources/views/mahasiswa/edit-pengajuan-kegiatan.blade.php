<x-app title="Edit Pengajuan Kegiatan">
    <div class="page-heading">
        <h3>Edit Pengajuan Kegiatan</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('pengajuan-kegiatan.update',$kegiatan->id) }}" method="POST" class="mt-4"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <label class="badge bg-primary p-2" style="font-size: 16px;">Form Edit Pengajuan Kegiatan</label>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Judul Kegiatan <span class="text-danger">*</span></label>
                                <input type="text" name="judul" class="form-control @error('judul')
                                is-invalid
                            @enderror" value="{{ $kegiatan->judul }}" placeholder="cth: Seminar Nasional" required>
                                @error('judul')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Judul Kegiatan (English) <span class="text-danger">*</span></label>
                                <input type="text" name="judul_english" class="form-control @error('judul_english')
                                is-invalid
                            @enderror" value="{{ $kegiatan->judul_english }}" placeholder="cth: International Workshop"
                                    required>
                                @error('judul_english')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Tingkat <span class="text-danger">*</span></label>
                                <select name="tingkat" class="form-control @error('tingkat')
                                is-invalid
                            @enderror" required>
                                    <option value="">-- Pilih Tingkat --</option>
                                    <option value="Internasional" {{ $kegiatan->tingkat == 'Internasional' ?
                                        'selected':'' }}>Internasional</option>
                                    <option value="Nasional" {{ $kegiatan->tingkat == 'Nasional' ?
                                        'selected':'' }}>Nasional</option>
                                    <option value="Wilayah" {{ $kegiatan->tingkat == 'Wilayah' ?
                                        'selected':'' }}>Wilayah</option>
                                </select>
                                @error('tingkat')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Partisipasi <span class="text-danger">*</span></label>
                                <select name="partisipasi" class="form-control @error('partisipasi')
                                is-invalid
                            @enderror" required>
                                    <option value="">-- Pilih Partisipasi --</option>
                                    <option value="Ketua" {{ $kegiatan->partisipasi == 'Ketua' ?
                                        'selected':'' }}>Ketua</option>
                                    <option value="Anggota" {{ $kegiatan->partisipasi == 'Anggota' ?
                                        'selected':'' }}>Anggota</option>
                                    <option value="Peserta" {{ $kegiatan->partisipasi == 'Peserta' ?
                                        'selected':'' }}>Peserta</option>
                                </select>
                                @error('partisipasi')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Penyelenggara <span class="text-danger">*</span></label>
                                <input type="text" name="penyelenggara" class="form-control @error('penyelenggara')
                                is-invalid
                            @enderror" value="{{ $kegiatan->penyelenggara }}"
                                    placeholder="cth: STIE Cendekia Karya Utama" required>
                                @error('penyelenggara')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Tahun <span class="text-danger">*</span></label>
                                <input type="text" name="tahun" class="form-control @error('tahun')
                                is-invalid
                            @enderror" value="{{ $kegiatan->tahun }}" required>
                                @error('tahun')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>File <span class="text-danger">*</span> ( <a
                                        href="{{ Storage::url($kegiatan->file) }}" class="text-sm" target="_blank">
                                        Lihat
                                        File</a> )
                                </label>
                                <input type="file" name="file" class="form-control @error('file')
                                is-invalid
                            @enderror">
                                @error('file')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>URL Kegiatan <span>(Opsional)</span></label>
                                <input type="text" name="url" class="form-control" value="{{ $kegiatan->url }}">
                            </div>
                        </div>
                    </div>
                    <div class="float-end">
                        <a href="{{ route('daftar-pengajuan') }}" class="btn btn-sm btn-light"><i
                                class="bi bi-arrow-left"></i>
                            Back</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-send-fill"></i>
                            Ajukan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app>