<x-app title="Edit Pengajuan Prestasi">
    <div class="page-heading">
        <h3>Edit Pengajuan Prestasi</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('prestasi.update',$prestasi->id) }}" method="POST" class="mt-4"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <label class="badge bg-primary p-2" style="font-size: 16px;">Form Edit Pengajuan Prestasi</label>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Mahasiswa <span class="text-danger">*</span></label>
                                <input type="text" name="mahasiswa_id" class="form-control @error('mahasiswa_id')
                                is-invalid
                            @enderror" value="{{ $prestasi->mahasiswa->nama }}" placeholder="cth: Seminar Nasional"
                                    required disabled>
                                @error('mahasiswa_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Judul Prestasi <span class="text-danger">*</span></label>
                                <input type="text" name="judul" class="form-control @error('judul')
                                            is-invalid
                                        @enderror" value="{{ $prestasi->judul }}" placeholder="cth: Cerdas Cermat"
                                    required disabled>
                                @error('judul')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Judul Prestasi (English)<span class="text-danger">*</span></label>
                                <input type="text" name="judul_english" class="form-control @error('judul_english')
                                            is-invalid
                                        @enderror" value="{{ $prestasi->judul_english }}"
                                    placeholder="cth: Cerdas Cermat" required disabled>
                                @error('judul_english')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Tingkat <span class="text-danger">*</span></label>
                                <select name="tingkat" class="form-control @error('tingkat')
                                            is-invalid
                                        @enderror" required disabled>
                                    <option value="">-- Pilih Tingkat --</option>
                                    <option value="Internasional" {{ $prestasi->tingkat == 'Internasional' ?
                                        'selected':'' }}>Internasional</option>
                                    <option value="Nasional" {{ $prestasi->tingkat == 'Nasional' ?
                                        'selected':'' }}>Nasional</option>
                                    <option value="Wilayah" {{ $prestasi->tingkat == 'Wilayah' ?
                                        'selected':'' }}>Wilayah</option>
                                </select>
                                @error('tingkat')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Peringkat <span class="text-danger">*</span></label>
                                <select name="peringkat" class="form-control @error('peringkat')
                                            is-invalid
                                        @enderror" required disabled>
                                    <option value="">-- Pilih Peringkat --</option>
                                    <option value="1" {{ $prestasi->peringkat == 1 ?
                                        'selected':'' }}>1</option>
                                    <option value="2" {{ $prestasi->peringkat == 2 ?
                                        'selected':'' }}>2</option>
                                    <option value="3" {{ $prestasi->peringkat == 3 ?
                                        'selected':'' }}>3</option>
                                </select>
                                @error('peringkat')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Penyelenggara <span class="text-danger">*</span></label>
                                <input type="text" name="penyelenggara" class="form-control @error('penyelenggara')
                                            is-invalid
                                        @enderror" value="{{ $prestasi->penyelenggara }}"
                                    placeholder="cth: STIE Cendekia Karya Utama" required disabled>
                                @error('penyelenggara')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tahun <span class="text-danger">*</span></label>
                                <input type="text" name="tahun" class="form-control @error('tahun')
                                            is-invalid
                                        @enderror" value="{{ $prestasi->tahun }}" required disabled>
                                @error('tahun')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>File <span class="text-danger">*</span>( <a
                                        href="{{ Storage::url($prestasi->file) }}" class="text-sm" target="_blank">
                                        Lihat
                                        File</a> )</label>
                                <input type="file" name="file" class="form-control @error('file')
                                            is-invalid
                                        @enderror" disabled>
                                @error('file')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>URL Kegiatan <span>(Opsional)</span></label>
                                <input type="text" name="url" class="form-control" value="{{ $prestasi->url }}"
                                    disabled>
                            </div>
                            <div class="form-group">
                                <label>Status Pengajuan</label>
                                <input type="text" name="status_pengajuan" class="form-control"
                                    value="{{ $prestasi->status_pengajuan }}" disabled>
                            </div>
                            <div class="form-group">
                                <label>Status Validasi</label>
                                <select name="status_validasi"
                                    class="form-control @error('status_validasi') is-invalid @enderror">
                                    <option value="Menunggu Validasi" {{ $prestasi->status_validasi == 'Menunggu
                                        Validasi' ? 'selected':'' }}>Menunggu Validasi</option>
                                    <option value="Tervalidasi" {{ $prestasi->status_validasi == 'Tervalidasi' ?
                                        'selected':'' }}>Tervalidasi</option>
                                    <option value="Ditolak" {{ $prestasi->status_validasi == 'Ditolak' ?
                                        'selected':'' }}>Ditolak</option>
                                </select>
                                @error('status_validasi')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Komentar</label>
                                <textarea name="komentar" class="form-control"
                                    rows="3">{{ $prestasi->komentar }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="float-end">
                        <a href="{{ route('prestasi.index') }}" class="btn btn-sm btn-light"><i
                                class="bi bi-arrow-left"></i> Back</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-send-fill"></i>
                            Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app>