<x-app title="Add Mahasiswa">
    <div class="page-heading">
        <h3>Mahasiswa</h3>
    </div>
    <div class="page-content">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-titl">Add Mahasiswa</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('mahasiswa.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Nama Lengkap</label>
                                    <input type="text" name="nama" class="form-control @error('nama')
                                        is-invalid
                                    @enderror" value="{{ old('nama') }}" required>
                                    @error('nama')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">NIM</label>
                                    <input type="text" name="nim" class="form-control @error('nim')
                                        is-invalid
                                    @enderror" value="{{ old('nim') }}" required>
                                    @error('nim')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Tempat Lahir</label>
                                    <input type="text" name="tempat_lahir" class="form-control @error('tempat_lahir')
                                        is-invalid
                                    @enderror" value="{{ old('tempat_lahir') }}" required>
                                    @error('tempat_lahir')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Tanggal Lahir</label>
                                    <input type="date" name="tanggal_lahir" class="form-control @error('tanggal_lahir')
                                        is-invalid
                                    @enderror" required>
                                    @error('tanggal_lahir')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Kelas</label>
                                    <select name="kelas" class="form-control @error('kelas')
                                            is-invalid
                                        @enderror" required>
                                        <option value="">-- Pilih --</option>
                                        <option value="Pagi" {{ old('kelas')=='Pagi' ? 'selected' : '' }}>Pagi
                                        </option>
                                        <option value="Sore" {{ old('kelas')=='Sore' ? 'selected' : '' }}>Sore
                                        </option>
                                        <option value="Weekend" {{ old('kelas')=='Weekend' ? 'selected' : '' }}>
                                            Weekend
                                        </option>
                                    </select>
                                    @error('kelas')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Program Studi</label>
                                    <select name="program_studi" class="form-control @error('program_studi')
                                            is-invalid
                                        @enderror" required>
                                        <option value="">-- Pilih --</option>
                                        <option value="S1 - Akuntansi" {{ old('program_studi')=='S1 - Akuntansi'
                                            ? 'selected' : '' }}>S1 -
                                            Akuntansi</option>
                                        <option value="S1 - Manajemen" {{ old('program_studi')=='S1 - Manajemen'
                                            ? 'selected' : '' }}>S1 -
                                            Manajemen</option>
                                    </select>
                                    @error('program_studi')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Nomor Ijazah</label>
                                    <input type="text" name="nomor_ijazah" class="form-control @error('nomor_ijazah')
                                        is-invalid
                                    @enderror" value="{{ old('nomor_ijazah') }}">
                                    @error('nomor_ijazah')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Tanggal Kelulusan</label>
                                    <input type="date" name="tanggal_kelulusan" class="form-control @error('tanggal_kelulusan')
                                        is-invalid
                                    @enderror">
                                    @error('tanggal_kelulusan')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" name="email" class="form-control @error('email')
                                        is-invalid
                                    @enderror" value="{{ old('email') }}" required>
                                    @error('email')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="float-end mt-3">
                            <a href="{{ route('mahasiswa.index') }}" class="btn btn-sm btn-secondary"><i
                                    class="bi bi-arrow-left"></i> Back</a>
                            <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-check"></i>
                                Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</x-app>