<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SKPI - {{ $mahasiswa->nama }}</title>

    <link rel="stylesheet" href="{{ asset('assets/compiled/css/app.css') }}">
    <style>
        table,
        tr,
        th {
            border: 1px solid black;
            text-align: justify;
            font-size: 15px;
        }

        table {
            border-collapse: collapse;
            width: 95%;
        }

        td {
            vertical-align: top;
        }
    </style>
</head>

<body>
    <div class="header text-center">
        <img src="{{ asset('img/logo.png') }}" class="img-fluid" width="100" alt="">
        <h2 class="mt-3">STIE CENDEKIA KARYA UTAMA</h2>
        <h3 class="mt-5">Diploma Supplement</h3>
        <h2>Surat Keterangan Pendamping Ijazah</h2>
        <p>Nomor : CKU-SKPI{{ $mahasiswa->nim }}{{ $mahasiswa->nomor_ijazah }}{{ $mahasiswa->id }}</p>
        <p class="mt-3 fst-italic">The Diploma Supplement accompanies a higher education certificate providing a
            standardized<br>description of
            the
            nature, level, context, content and status of the studies completed by its holder</p>
        <p>Surat Keterangan Pendamping Ijazah sebagai pelengkap Ijazah yang menerangkan<br>capaian pembelajaran dan
            prestasi dari pemegang Ijazah selama masa studi</p>
    </div>
    <div class="content mt-4">
        <ol type="I">
            <li>
                INFORMASI TENTANG IDENTITAS DIRI PEMEGANG SKPI<br>
                <i>Information of Personal Information Diploma Supplement Holder</i>
            </li>
            <table class="mt-3 mb-3">
                <tr>
                    <td>1.1</td>
                    <td>
                        Nama Mahasiswa<br>
                        <i>Name of Student</i>
                    </td>
                    <td>:</td>
                    <td>{{ $mahasiswa->nama }}</td>
                </tr>
                <tr>
                    <td>1.2</td>
                    <td>
                        Nama Induk Mahasiswa<br>
                        <i>Student ID Number</i>
                    </td>
                    <td>:</td>
                    <td>{{ $mahasiswa->nim }}</td>
                </tr>
                <tr>
                    <td>1.3</td>
                    <td>
                        Tempat dan Tanggal Lahir<br>
                        <i>Place and Date of Birth</i>
                    </td>
                    <td>:</td>
                    <td>{{ $mahasiswa->tempat_lahir }}, {{
                        \Carbon\Carbon::parse($mahasiswa->tanggal_lahir)->isoFormat('D MMMM Y') }}</td>
                </tr>
                <tr>
                    <td>1.4</td>
                    <td>
                        Program Pendidikan<br>
                        <i>Education Program</i>
                    </td>
                    <td>:</td>
                    <td>
                        Sarjana<br>
                        <i>Undergraduate</i>
                    </td>
                </tr>
                <tr>
                    <td>1.5</td>
                    <td>
                        Program Studi<br>
                        <i>Study Program</i>
                    </td>
                    <td>:</td>
                    <td>
                        @if ($mahasiswa->program_studi == 'S1 - Akuntansi')
                        Akuntansi<br>
                        <i>Accountancy</i>
                        @else
                        Manajemen<br>
                        <i>Management</i>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>1.6</td>
                    <td>
                        Gelar<br>
                        <i>Degree</i>
                    </td>
                    <td>:</td>
                    <td>
                        @if ($mahasiswa->program_studi == 'S1 - Akuntansi')
                        Sarjana Akuntansi (S.Akt)
                        @else
                        Sarjana Manajemen (S.M)
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>1.7</td>
                    <td>
                        Nomor Ijazah<br>
                        <i>Number of Certification</i>
                    </td>
                    <td>:</td>
                    <td>
                        {{ $mahasiswa->nomor_ijazah }}
                    </td>
                </tr>
                <tr>
                    <td style="width: 7%;">1.8</td>
                    <td style="width: 45%;">
                        Tanggal Kelulusan<br>
                        <i>Date of Graduation</i>
                    </td>
                    <td style="width: 2%;">:</td>
                    <td style="width: 46%;">
                        @if ($mahasiswa->tanggal_kelulusan)
                        {{ \Carbon\Carbon::parse($mahasiswa->tanggal_kelulusan)->isoFormat('D MMMM Y') }}
                        @endif
                    </td>
                </tr>
            </table>
            <li>
                INFORMASI TENTANG IDENTITAS PENYELENGGARA PROGRAM<br>
                <i>Information of Identity Higher Education Institution</i>
                <table class="mt-3 mb-3">
                    <tr>
                        <td>2.1</td>
                        <td>
                            Nama Perguruan Tinggi<br>
                            <i>Name of University</i>
                        </td>
                        <td>:</td>
                        <td>STIE Cendekia Karya Utama</td>
                    </tr>
                    <tr>
                        <td>2.2</td>
                        <td>
                            Surat Keputusan Pendirian<br>
                            <i>Certificate of Establishment</i>
                        </td>
                        <td>:</td>
                        <td>Keputusan Menteri Pendidikan dan Kebudayaan Republik Indonesia Nomor 11/D/O/1996 tanggal 3
                            April 1996</td>
                    </tr>
                    <tr>
                        <td>2.3</td>
                        <td>
                            Jenjang Kualifikasi Sesuai KKNI<br>
                            <i>Appropriate Level of Qualification KKNI</i>
                        </td>
                        <td>:</td>
                        <td>Level 6</td>
                    </tr>
                    <tr>
                        <td>2.4</td>
                        <td>
                            Persyaratan Penerimaan<br>
                            <i>Admission Requirements</i>
                        </td>
                        <td>:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>2.5</td>
                        <td>
                            Bahasa Pengantar Kuliah<br>
                            <i>Language Study</i>
                        </td>
                        <td>:</td>
                        <td>Bahasa Indonesia</td>
                    </tr>
                    <tr>
                        <td>2.6</td>
                        <td>
                            Sistem Penilaian<br>
                            <i>Course Grading System</i>
                        </td>
                        <td>:</td>
                        <td>A = 4; B = 3; C = 2; D = 1; E = 0</td>
                    </tr>
                    <tr>
                        <td>2.7</td>
                        <td>
                            Lama Studi Regular<br>
                            <i>Regular Study Period</i>
                        </td>
                        <td>:</td>
                        <td>
                            Delapan Semester<br>
                            <i>Eight Semester</i>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 7%;">2.8</td>
                        <td style="width: 45%;">
                            Jenis dan Jenjang Pendidikan Lanjutan<br>
                            <i>Access to Further Study</i>
                        </td>
                        <td style="width: 2%;">:</td>
                        <td style="width: 46%;">
                            Magister dan Doktor<br>
                            <i>Master and Doctor</i>
                        </td>
                    </tr>
                </table>
            </li>
            <li>
                INFORMASI TENTANG KUALIFIKASI DAN HASIL YANG DICAPAI<br>
                <i>Information of Qualification and Learning Outcome</i>
                <table class="mt-3 mb-3">
                    <tr>
                        <td>3.1</td>
                        <td colspan="3">
                            Capaian Pembelajaran<br>
                            <i>Learning Outcome</i>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 7%;">3.1.1</td>
                        <td style="width: 45%;">Bertaqwa kepada Tuhan Yang Maha Esa dan mampu menunjukkan sikap
                            religious
                        </td>
                        <td style="width: 2%;"></td>
                        <td style="width: 46%;"><i>Be devoted to God Almighty and be able to show a religious
                                attitude</i></td>
                    </tr>
                    <tr>
                        <td>3.1.2</td>
                        <td>Menjunjung tinggi nilai kemanusiaan dalam menjalankan tugas berdasarkan agama, moral dan
                            etika
                        </td>
                        <td></td>
                        <td><i>Upholding human values in carrying out duties based on religion, morals and ethics</i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.3</td>
                        <td>Berkontribusi dalam peningkatan mutu kehidupan bermasyarakat, berbangsa dan bernegara dan
                            kemajuan peradaban berdasarkan Pancasila
                        </td>
                        <td></td>
                        <td>
                            <i>Contribute to improving the quality of life in society, nation and state and the progress
                                of civilization based on Pancasila</i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.4</td>
                        <td>Menunjukkan sikap bertanggungjawab atas pekerjaan di bidang keahliannya secara mandiri
                        </td>
                        <td></td>
                        <td>
                            <i>Demonstrate a responsible attitude towards work in their field of expertise
                                independently</i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.5</td>
                        <td>Menginternalisasi semangat kemandirian, keuangan dan kewirausahaan
                        </td>
                        <td></td>
                        <td>
                            <i>Internalize the spirit of independence, finance and entrepreneurship</i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.6</td>
                        <td>Menguasai konsep dasar teoretik dan memiliki kemampuan profesional dalam bidang logistik
                        </td>
                        <td></td>
                        <td>
                            <i>Master basic theoretical concepts and have professional skills in the field of
                                logistics</i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.7</td>
                        <td>
                            Mampu menerapkan pemikiran logis, kritis, sistematis, dan inovatif dalam konteks
                            pengembangan atau implementasi ilmu pengetahuan dan teknologi yang memperhatikan dan
                            menerapkan nilai humaniora yang sesuai dengan bidang keahliannya
                        </td>
                        <td></td>
                        <td>
                            <i>
                                Able to apply logical, critical, systematic and innovative thinking in the context of
                                developing or implementing science and technology that pays attention to and applies
                                humanities values in accordance with their field of expertise
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.8</td>
                        <td>
                            Mampu menunjukkan kinerja mandiri, bermutu, dan terukur
                        </td>
                        <td></td>
                        <td>
                            <i>
                                Able to demonstrate independent, quality and measurable performance
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.9</td>
                        <td>
                            Mampu mengkaji implikasi pengembangan atau implementasi ilmu pengetahuan teknologi yang
                            memperhatikan dan menerapkan nilai humaniora sesuai dengan keahliannya berdasarkan kaidah,
                            tata cara, dan etika ilmiah dalam rangka menghasilkan solusi, gagasan, desain atau analisis
                            kritik, serta menyusun deskripsi saintifik hasil kajiannya dalam bentuk skripsi atau laporan
                            tugas akhir, dan mengunggahnya dalam laman perguruan tinggi
                        </td>
                        <td></td>
                        <td>
                            <i>
                                Able to study the implications of developing or implementing technological science by
                                paying attention to and applying humanities values in accordance with their expertise
                                based on scientific rules, procedures and ethics in order to produce solutions, ideas,
                                designs or critical analysis, as well as compiling scientific descriptions of the
                                results of their studies in the form of a thesis or report final assignment, and upload
                                it on the college page </i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.10</td>
                        <td>
                            Memiliki kemampuan logika berpikir ilmiah untuk mengkaji fenomena secara teoritik maupun
                            empirik dalam beberapa cabang ilmu yang termasuk dalam Rumpun Ilmu Alam, Humaniora, Sosial,
                            dan Terapan
                        </td>
                        <td></td>
                        <td>
                            <i>
                                Have the ability to logically think scientifically to study phenomena theoretically and
                                empirically in several branches of science included in the Natural, Humanities, Social
                                and Applied Sciences</i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.11</td>
                        <td>
                            Mampu melakukan riset tingkat pemula dengan menggunakan logika berpikir ilmiah untuk
                            memberikan alternatif penyelesaian masalah terkait dengan beberapa cabang ilmu yang termasuk
                            dalam Rumpun Ilmu Alam, Humaniora, Sosial, dan Terapan secara mendalam, serta mampu
                            memformulasikan penyelesaian masalah prosedural dalam bidang tersebut
                        </td>
                        <td></td>
                        <td>
                            <i>
                                Able to carry out beginner level research using logical scientific thinking to provide
                                alternative solutions to problems related to several branches of science included in the
                                Natural, Humanities, Social and Applied Sciences Group in depth, and able to formulate
                                procedural problem solving in these fields</i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.1.12</td>
                        <td>
                            Mampu berkomunikasi secara lisan dan tertulis, serta membangun hubungan interpersonal yang
                            produktif berbasis ipteks, dan potensi lingkungan setempat, sesuai standar proses dan mutu
                        </td>
                        <td></td>
                        <td>
                            <i>
                                Able to communicate verbally and in writing, as well as building productive
                                interpersonal relationships based on science and technology, and the potential of the
                                local environment, according to process and quality standards</i>
                        </td>
                    </tr>
                    <tr>
                        <td>3.2</td>
                        <td colspan="3">
                            Informasi Tambahan<br>
                            <i>Additional Information</i>
                        </td>
                    </tr>
                    @php
                    $counter = 1;
                    @endphp
                    @foreach ($kegiatans as $kegiatan)
                    <tr>
                        <td>3.2.{{ $counter }}</td>
                        <td>{{ $kegiatan->judul }}
                        </td>
                        <td></td>
                        <td><i>{{ $kegiatan->judul_english }}</i>
                        </td>
                    </tr>
                    @php
                    $counter++;
                    @endphp
                    @endforeach
                    @foreach ($prestasis as $prestasi)
                    <tr>
                        <td>3.2.{{ $counter }}</td>
                        <td>{{ $prestasi->judul }}
                        </td>
                        <td></td>
                        <td><i>{{ $prestasi->judul_english }}</i>
                        </td>
                    </tr>
                    @php
                    $counter++;
                    @endphp
                    @endforeach
                    @foreach ($kompetensis as $kompetensi)
                    <tr>
                        <td>3.2.{{ $counter }}</td>
                        <td>{{ $kompetensi->judul }}
                        </td>
                        <td></td>
                        <td><i>{{ $kompetensi->judul_english }}</i>
                        </td>
                    </tr>
                    @php
                    $counter++;
                    @endphp
                    @endforeach
                </table>
                <div class="sign text-center mt-4">
                    @php
                    $date = date("Y-m-d", strtotime($mahasiswa->tanggal_kelulusan . " +7 days"));
                    $yudisium = \Carbon\Carbon::parse($date)->isoFormat('D MMMM Y');
                    @endphp
                    <p style="margin-left: 300px;">
                        Semarang, {{ $yudisium }}<br>
                        Ketua<br>
                        STIE Cendekia Karya Utama<br><br><br><br><br><br>
                        Drs. Dirgo Wahyono, M.Si<br>
                        NIK. 21000001
                    </p>
                </div>
            </li>
        </ol>
    </div>
</body>

</html>