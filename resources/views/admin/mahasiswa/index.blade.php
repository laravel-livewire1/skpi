<x-app title="Mahasiswa">
    <div class="page-heading">
        <h3>Mahasiswa</h3>
    </div>
    <div class="page-content">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        Data Mahasiswa
                    </h5>
                    <div class="float-end" style="margin-top: -35px;">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-sm btn-success" data-bs-toggle="modal"
                            data-bs-target="#importModal">
                            <i class="bi bi-file-excel"></i> Import
                        </button>
                        <a href="{{ route('mahasiswa.create') }}" class="btn btn-sm btn-primary"><i
                                class="bi bi-plus"></i>
                            Add</a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1" style="font-size: 14px;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Program Studi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mahasiswas as $mahasiswa)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <a href="{{ route('mahasiswa.show',$mahasiswa->id) }}">{{ $mahasiswa->nim }}</a>
                                </td>
                                <td>{{ $mahasiswa->nama }}</td>
                                <td>{{ $mahasiswa->kelas }}</td>
                                <td>{{ $mahasiswa->program_studi }}</td>
                                <td>
                                    <a href="{{ route('mahasiswa.edit', $mahasiswa->id) }}"
                                        class="btn btn-sm btn-warning"><i class="bi bi-pencil"></i>
                                        Edit</a>
                                    <form action="{{ route('mahasiswa.destroy', $mahasiswa->id) }}" method="POST"
                                        class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger"
                                            onclick="return confirm('Yakin ingin menghapus data ini ?')"><i
                                                class="bi bi-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="importModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Import Data Mahasiswa</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('mahasiswa.import') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>File <a href="{{ asset('excel/format-import-mahasiswa.xlsx') }}"
                                    style="font-size: 14px;" download> ( Format File)</a></label>
                            <input type="file" class="form-control @error('file')
                                is-invalid
                            @enderror" name="file" required>
                            @error('file')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal"><i
                                class="bi bi-x"></i> Close</button>
                        <button type="submit" class="btn btn-success btn-sm"><i class="bi bi-check"></i> Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app>

<link rel="stylesheet" href="{{ asset('assets/extensions/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}">
<link rel="stylesheet" crossorigin href="{{ asset('assets/compiled/css/table-datatable-jquery.css') }}">
<script src="{{ asset('assets/extensions/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/extensions/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/extensions/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('assets/static/js/pages/datatables.js') }}"></script>