<x-app title="Pengajuan Kegiatan">
    <div class="page-heading">
        <h3>Pengajuan Kegiatan</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1" style="font-size: 14px;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Mahasiswa</th>
                            <th>Judul</th>
                            <th>Tingkat</th>
                            <th>Pengajuan</th>
                            <th>Validasi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kegiatans as $kegiatan)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $kegiatan->mahasiswa->nama }}</td>
                            <td>
                                <a href="{{ Storage::url($kegiatan->file) }}" target="_blank">{{
                                    $kegiatan->judul }}</a>
                            </td>
                            <td>{{ $kegiatan->tingkat }}</td>
                            <td>
                                <span class="badge bg-info">{{ $kegiatan->status_pengajuan }}</span>
                            </td>
                            <td>
                                @if ($kegiatan->status_validasi == 'Tervalidasi')
                                <span class="badge bg-success">{{ $kegiatan->status_validasi }}</span>
                                @elseif($kegiatan->status_validasi == 'Menunggu Validasi')
                                <span class="badge bg-info">{{ $kegiatan->status_validasi }}</span>
                                @else
                                <span class="badge bg-danger">{{ $kegiatan->status_validasi }}</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('kegiatan.edit',$kegiatan->id) }}" class="btn btn-sm btn-warning"><i
                                        class="bi bi-pencil"></i> Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app>

<link rel="stylesheet" href="{{ asset('assets/extensions/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}">
<link rel="stylesheet" crossorigin href="{{ asset('assets/compiled/css/table-datatable-jquery.css') }}">
<script src="{{ asset('assets/extensions/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/extensions/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/extensions/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('assets/static/js/pages/datatables.js') }}"></script>
