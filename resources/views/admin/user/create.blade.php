<x-app title="Add Pengguna">
    <div class="page-heading">
        <h3>Pengguna</h3>
    </div>
    <div class="page-content">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-titl">Add Pengguna</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('user.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="name"
                                class="form-control @error('name')
                                is-invalid
                            @enderror"
                                value="{{ old('name') }}" required>
                            @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" name="email"
                                class="form-control @error('email')
                                is-invalid
                            @enderror"
                                value="{{ old('email') }}" required>
                            @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="password"
                                class="form-control @error('password')
                                is-invalid
                            @enderror"
                                required>
                            @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Role</label>
                            <input type="text" class="form-control" value="admin" disabled>
                        </div>
                        <div class="float-end mt-3">
                            <a href="{{ route('user.index') }}" class="btn btn-sm btn-secondary"><i
                                    class="bi bi-arrow-left"></i> Back</a>
                            <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-check"></i>
                                Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</x-app>
