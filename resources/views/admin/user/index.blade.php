<x-app title="Pengguna">
    <div class="page-heading">
        <h3>Pengguna</h3>
    </div>
    <div class="page-content">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        Data Pengguna
                    </h5>
                    <div class="float-end" style="margin-top: -35px;">
                        <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary"><i class="bi bi-plus"></i>
                            Add</a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1" style="font-size: 14px;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ strtoupper($user->role) }}</td>
                                <td>
                                    @if ($user->status == 1)
                                    <span class="badge bg-success">
                                        Aktif
                                    </span>
                                    @else
                                    <span class="badge bg-danger">
                                        Tidak Aktif
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('user.edit', $user->id) }}" class="btn btn-sm btn-warning"><i
                                            class="bi bi-pencil"></i>
                                        Edit</a>
                                    <form action="{{ route('user.destroy', $user->id) }}" method="POST"
                                        class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger"
                                            onclick="return confirm('Yakin ingin menghapus data ini ?')"><i
                                                class="bi bi-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</x-app>

<link rel="stylesheet" href="{{ asset('assets/extensions/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}">
<link rel="stylesheet" crossorigin href="{{ asset('assets/compiled/css/table-datatable-jquery.css') }}">
<script src="{{ asset('assets/extensions/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/extensions/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/extensions/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('assets/static/js/pages/datatables.js') }}"></script>
