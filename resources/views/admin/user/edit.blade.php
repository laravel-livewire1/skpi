<x-app title="Edit Pengguna">
    <div class="page-heading">
        <h3>Pengguna</h3>
    </div>
    <div class="page-content">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-titl">Edit Pengguna</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('user.update', $user->id) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="name" class="form-control @error('name')
                                is-invalid
                            @enderror" value="{{ $user->name }}" required>
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" name="email" class="form-control @error('email')
                                is-invalid
                            @enderror" value="{{ $user->email }}" required>
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Password <span style="font-size: 12px;">( hanya diinput saat ingin ganti
                                    password )</span></label>
                            <input type="password" name="password" class="form-control @error('password')
                                is-invalid
                            @enderror">
                            @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Role</label>
                            <input type="text" class="form-control" value="{{ $user->role }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="">Status</label>
                            <select name="status" class="form-control @error('status')
                                is-invalid
                            @enderror" required>
                                <option value="0" {{ $user->status == 0 ? 'selected' : '' }}>Tidak Aktif</option>
                                <option value="1" {{ $user->status == 1 ? 'selected' : '' }}>Aktif</option>
                            </select>
                            @error('status')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="float-end mt-3">
                            <a href="{{ route('user.index') }}" class="btn btn-sm btn-secondary"><i
                                    class="bi bi-arrow-left"></i> Back</a>
                            <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-check"></i>
                                Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</x-app>
