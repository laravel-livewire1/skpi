<x-app title="Pengajuan Kompetensi">
    <div class="page-heading">
        <h3>Pengajuan Kompetensi</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1" style="font-size: 14px;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Mahasiswa</th>
                            <th>Judul</th>
                            <th>Bidang</th>
                            <th>Pengajuan</th>
                            <th>Validasi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kompetensis as $kompetensi)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $kompetensi->mahasiswa->nama }}</td>
                            <td>
                                <a href="{{ Storage::url($kompetensi->file) }}" target="_blank">{{
                                    $kompetensi->judul }}</a>
                            </td>
                            <td>{{ $kompetensi->bidang }}</td>
                            <td>
                                <span class="badge bg-info">{{ $kompetensi->status_pengajuan }}</span>
                            </td>
                            <td>
                                @if ($kompetensi->status_validasi == 'Tervalidasi')
                                <span class="badge bg-success">{{ $kompetensi->status_validasi }}</span>
                                @elseif($kompetensi->status_validasi == 'Menunggu Validasi')
                                <span class="badge bg-info">{{ $kompetensi->status_validasi }}</span>
                                @else
                                <span class="badge bg-danger">{{ $kompetensi->status_validasi }}</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('kompetensi.edit',$kompetensi->id) }}"
                                    class="btn btn-sm btn-warning"><i class="bi bi-pencil"></i> Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app>

<link rel="stylesheet" href="{{ asset('assets/extensions/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}">
<link rel="stylesheet" crossorigin href="{{ asset('assets/compiled/css/table-datatable-jquery.css') }}">
<script src="{{ asset('assets/extensions/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/extensions/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/extensions/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('assets/static/js/pages/datatables.js') }}"></script>
