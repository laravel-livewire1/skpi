<x-app title="Edit Pengajuan Kompetensi">
    <div class="page-heading">
        <h3>Edit Pengajuan Kompetensi</h3>
    </div>
    <div class="page-content">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('kompetensi.update',$kompetensi->id) }}" method="POST" class="mt-4"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <label class="badge bg-primary p-2" style="font-size: 16px;">Form Edit Pengajuan
                        Kompetensi</label>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Mahasiswa <span class="text-danger">*</span></label>
                                <input type="text" name="mahasiswa_id" class="form-control @error('mahasiswa_id')
                                            is-invalid
                                        @enderror" value="{{ $kompetensi->mahasiswa->nama }}"
                                    placeholder="cth: Seminar Nasional" required disabled>
                                @error('mahasiswa_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Judul Kompetensi <span class="text-danger">*</span></label>
                                <input type="text" name="judul" class="form-control @error('judul')
                                            is-invalid
                                        @enderror" value="{{ $kompetensi->judul }}" placeholder="cth: Seminar Nasional"
                                    required disabled>
                                @error('judul')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Judul Kompetensi (English)<span class="text-danger">*</span></label>
                                <input type="text" name="judul_english" class="form-control @error('judul_english')
                                            is-invalid
                                        @enderror" value="{{ $kompetensi->judul_english }}"
                                    placeholder="cth: Seminar Nasional" required disabled>
                                @error('judul_english')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Bidang <span class="text-danger">*</span></label>
                                <input type="text" name="bidang" class="form-control @error('bidang')
                                            is-invalid
                                        @enderror" value="{{ $kompetensi->bidang }}" placeholder="cth: Logistik"
                                    required disabled>
                                @error('bidang')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Penyelenggara <span class="text-danger">*</span></label>
                                <input type="text" name="penyelenggara" class="form-control @error('penyelenggara')
                                            is-invalid
                                        @enderror" value="{{ $kompetensi->penyelenggara }}"
                                    placeholder="cth: STIE Cendekia Karya Utama" required disabled>
                                @error('penyelenggara')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Kelulusan <span class="text-danger">*</span></label>
                                <input type="date" name="tanggal_kelulusan" class="form-control @error('tanggal_kelulusan')
                                            is-invalid
                                        @enderror" value="{{ date($kompetensi->tanggal_kelulusan) }}" required
                                    disabled>
                                @error('tanggal_kelulusan')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>File <span class="text-danger">*</span>( <a
                                        href="{{ Storage::url($kompetensi->file) }}" class="text-sm" target="_blank">
                                        Lihat
                                        File</a> )</label>
                                <input type="file" name="file" class="form-control @error('file')
                                            is-invalid
                                        @enderror" disabled>
                                @error('file')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>URL Kegiatan <span>(Opsional)</span></label>
                                <input type="text" name="url" class="form-control" value="{{ $kompetensi->url }}"
                                    disabled>
                            </div>
                            <div class="form-group">
                                <label>Status Pengajuan</label>
                                <input type="text" name="status_pengajuan" class="form-control"
                                    value="{{ $kompetensi->status_pengajuan }}" disabled>
                            </div>
                            <div class="form-group">
                                <label>Status Validasi</label>
                                <select name="status_validasi"
                                    class="form-control @error('status_validasi') is-invalid @enderror">
                                    <option value="Menunggu Validasi" {{ $kompetensi->status_validasi == 'Menunggu
                                        Validasi' ? 'selected':'' }}>Menunggu Validasi</option>
                                    <option value="Tervalidasi" {{ $kompetensi->status_validasi == 'Tervalidasi' ?
                                        'selected':'' }}>Tervalidasi</option>
                                    <option value="Ditolak" {{ $kompetensi->status_validasi == 'Ditolak' ?
                                        'selected':'' }}>Ditolak</option>
                                </select>
                                @error('status_validasi')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Komentar</label>
                                <textarea name="komentar" class="form-control"
                                    rows="3">{{ $kompetensi->komentar }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="float-end">
                        <a href="{{ route('kompetensi.index') }}" class="btn btn-sm btn-light"><i
                                class="bi bi-arrow-left"></i> Back</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-send-fill"></i>
                            Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app>